const Client = require('instagram-private-api').V1;
const _ = require('lodash');

const Secrets = require('./secrets');
const { like } = require('./actions');

var tags = ['developer', 'developers', 'developerlife'];
var hashTagIds = [];

const developerTagId = 17841562447117467;
const javaScriptHashTag = 'javascript';
// And go for login

let likeByHashtag = function (device, storage) {
    Client.Session.create(device, storage, Secrets.username, Secrets.password)
	.then(function(session) {
		return session;
	}).then(function(session) {
        var feed = new Client.Feed.TaggedMedia(session, 'developer', '3');
		return [feed.get(), session];
	}).spread(function(media, session) {
        const filteredPosts = _.filter(media, function(ith) {
            if (_.includes(ith._params.caption, javaScriptHashTag)) {
                return ith;
            }
        });
        console.log('filtered: ' + filteredPosts.length);
        _.each(filteredPosts, function(ith) {
            // console.log(ith.account.id);
            like(ith, session);
        });
    }).catch(function(e) {
        console.log(e);
    });

}
module.exports = {likeByHashtag};
