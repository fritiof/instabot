const Client = require('instagram-private-api').V1;
const _ = require('lodash');
const fetch = require('node-fetch');
const fs = require('fs');
const readlineSync = require('readline-sync');

const { comment } = require('./comment');

module.exports.repost = function (session, rl) {
    let url = readlineSync.question('media URL to repost: ')
    console.log(`Media entered: ${url}`);
    let caption = readlineSync.question('Caption to use: ')

    return Client.Media.getByUrl(session, url)
    .then(media => {
        let originalUser = media._params.user.username;
        console.log('Post by: ', media._params.user.username);

        if (typeof media._params.images[0].url === 'string') {

            return fetch(media._params.images[0].url)
                .then(function(res) {
                    console.log('fetched image');
                    return res.buffer();
                }).then(function(buffer) {
                    console.log('created buffer');
                    return Client.Upload.photo(session, buffer)
                        .then(function(upload) {
                            console.log('uploaded');
                            return Client.Media.configurePhoto(session, upload.params.uploadId, `${caption} Repost from @${originalUser}`);
                        })
                        .then(function(medium) {
                            console.log('post uploaded successfully');
                            return comment(session, medium);
                        })
                });

        } else {
            console.log('No url');
        }
    })
};

// // Media {
// //     domain: null,
// //     _events: {},
// //     _eventsCount: 0,
// //     _maxListeners: undefined,
// //     _session: Session { _device: [Object], _cookiesStore: [Object], _jar: [Object] },
// //     _params:
// //      { code: 'BTNf-PmDD2m',
// //        id: '1498994867778108838_1697296',
// //        likeCount: 22948,
// //        hasLiked: false,
// //        hasMoreComments: true,
// //        photoOfYou: false,
// //        originalWidth: 720,
// //        commentsDisabled: undefined,
// //        commentCount: 1008,
// //        originalHeight: 720,
// //        mediaType: 2,
// //        deviceTimestamp: 1492914096,
// //        webLink: 'https://www.instagram.com/p/BTNf-PmDD2m/',
// //        usertags: undefined,
// //        videoDuration: 59.593333,
// //        hasAudio: true,
// //        viewCount: 172750,
// //        video: [Object],
// //        caption: 'Worth sharing ... important not to forget - true #entrepreneur',
// //        takenAt: 1492914134000,
// //        images: [Object],
// //        videos: [Object] },
// //     location:
// //      Location {
// //        domain: null,
// //        _events: {},
// //        _eventsCount: 0,
// //        _maxListeners: undefined,
// //        _session: [Object],
// //        _params: [Object],
// //        id: 212988663 },
// //     previewComments: [ [Object], [Object] ],
// //     comments: [ [Object], [Object] ],
// //     account:
// //      Account {
// //        domain: null,
// //        _events: {},
// //        _eventsCount: 0,
// //        _maxListeners: undefined,
// //        _session: [Object],
// //        _params: [Object],
// //        id: 1697296 },
// //     id: '1498994867778108838_1697296' }
