const Client = require('instagram-private-api').V1;
const _ = require('lodash');
const fs = require('fs');
const readlineSync = require('readline-sync');

module.exports.uploadImage = function (session) {
    const path = readlineSync.question('full path to file to upload: (must be .jpg) ')
    const caption = readlineSync.question('Caption to use: ')

    return Client.Upload.photo(session, path)
    .then(function(upload) {
        console.log('Uploading');
        return Client.Media.configurePhoto(session, upload.params.uploadId, caption);
    })
    .then(function(medium) {

        console.log('Post uploaded.');

        if (readlineSync.keyInYN("Do you want to add a comment with hashtags specified in '../hashtags.txt'? ")) {
            if (medium.id) {
                fs.readFile('../hashtags.txt', 'utf8', function(err, hashtags) {
                    return Client.Comment.create(session, medium.params.id, hashtags)
                    .then(comment => {
                        console.log('Commented successfully with: ', comment._params.text);
                    })
                });
            } else {
                console.log('No post ID');
            }
        } else {
          console.log('Not commenting. All done.');
        }
    })
};
