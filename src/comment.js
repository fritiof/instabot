const Client = require('instagram-private-api').V1;
const _ = require('lodash');
const fetch = require('node-fetch');
const fs = require('fs');
const readlineSync = require('readline-sync');

module.exports.comment = function (session, medium) {
    return new Promise((resolve, reject) => {
        if (readlineSync.keyInYN("Do you want to add a comment with hashtags specified in '../hashtags.txt'? ")) {
            if (medium.id) {
                fs.readFile('../hashtags.txt', 'utf8', function(err, hashtags) {
                    if (err) return console.log(err);
                    Client.Comment.create(session, medium.params.id, hashtags)
                    .then(comment => {
                        console.log('Commented successfully with: ', comment._params.text);
                        Promise.resolve()
                    })
                });
            } else {
                console.log('No post ID');
                Promise.reject();
            }
        } else {
            console.log('Not commenting. All done.');
            Promise.resolve();
        }
    })
};
