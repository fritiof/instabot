const Client = require('instagram-private-api').V1;
const _ = require('lodash');
const fetch = require('node-fetch');
var readlineSync = require('readline-sync');
const fs = require('fs');

const { username, password } = require('./secrets');
const { repost } = require('./src/repost');
const { uploadImage } = require('./src/uploadimage');
const { comment } = require('./src/comment');

const device = new Client.Device(username);
const storage = new Client.CookieFileStorage('./cookies/user.json');
const menuItems = ['Repost', 'Upload single photo', 'Comment'],

menuHandler = (session, index) => {
    switch (menuItems[index]) {
        case 'Repost':
            console.log('Repost chosen');
            return repost(session)
            break
        case 'Upload single photo':
            console.log("Upload single photo chosen");
            return uploadImage(session)
            break
        case 'Comment':
            let url = readlineSync.question('media URL to comment: ')
            return Client.Media.getByUrl(session, url)
            .then(medium => comment(session, medium))
            break
        default:
            console.log(`Screw you, I'm going home!`);
    }
}

showMenuSelection = (session) => {
    const index = readlineSync.keyInSelect(menuItems, 'What would you like to do today?');
    return menuHandler(session, index)
}

console.log('Initiating session');
Client.Session.create(device, storage, username, password)
    .then(function(session) {
        showMenuSelection(session);
    })
	.catch(e => {
		console.log("failed:", e);
	})
